package java.sql;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;
import java.util.Properties;

public class DriverShim implements Driver {
  private Driver driver;
  DriverShim(Driver d) {
    this.driver = d;
  }

  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return driver.getParentLogger();
  }

  public boolean acceptsURL(String u) throws SQLException {
    return this.driver.acceptsURL(u);
  }

  public Connection connect(String u, Properties p) throws SQLException {
    return this.driver.connect(u, p);
  }

  public int getMajorVersion() {
    return this.driver.getMajorVersion();
  }

  public int getMinorVersion() {
    return this.driver.getMinorVersion();
  }

  public DriverPropertyInfo[] getPropertyInfo(String u, Properties p) throws SQLException {
    return this.driver.getPropertyInfo(u, p);
  }

  public boolean jdbcCompliant() {
    return this.driver.jdbcCompliant();
  }
}
