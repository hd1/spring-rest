package us.d8u;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.List;
import java.util.Properties;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationUI implements ActionListener {
    final JComboBox databaseType = new JComboBox();
    final JComboBox driverField = new JComboBox();
    // FIXME JSecureTextField?
    final JTextField passwordField = new JTextField(TEXTFIELD_WIDTH);
    final JTextField urlField = new JTextField(TEXTFIELD_WIDTH);
    final JTextField usernameField = new JTextField(TEXTFIELD_WIDTH);

  private static final int NUMBER_OF_FIELDS = 8;
  private static final Logger logger = LoggerFactory.getLogger(ConfigurationUI.class);
  private static final int TEXTFIELD_WIDTH = 90;

  public ConfigurationUI() {
    JLabel driverLabel = new JLabel("Jdbc Driver Class");
    // collect driver names
    // JDBC4 drivers must have META-INF/services/java.sql.Driver

    Enumeration<URL> roots = null;
    try {
	roots = getClass().getClassLoader().getResources("");
    } catch (IOException e) {
    }
    
    while (roots.hasMoreElements()) {
      URL url = roots.nextElement();
      
      File root = new File(url.getPath());

      JarInputStream classInputStream = null;
      try {
	  classInputStream = new JarInputStream(new FileInputStream(root));
      } catch (IOException e) {
	  return;
      }
      JarEntry nextEntry = null;
      do {
	  try {
	      nextEntry = classInputStream.getNextJarEntry();
	  } catch (IOException e) {
	      logger.error(e.getMessage(), e);
	  }
        if (nextEntry.getName().contains("java.sql.Driver")) {
          InputStream rawStream = getClass().getResourceAsStream(nextEntry.getName());
          BufferedReader reader = new BufferedReader(new InputStreamReader(rawStream));
	  try {
	      String driverClass = reader.readLine();
	      driverField.addItem(driverClass);
	  } catch (IOException e) {
	      logger.error(e.getMessage(), e);
	  }
        }
      } while (nextEntry != null);
    }

    driverField.setEditable(true);

    // FIXME fill template on change of above field
    JLabel urlLabel = new JLabel("Jdbc Url");

    // FIXME think about eliminating the next two and having them part of the url
    JLabel userLabel = new JLabel("Jdbc Username");

    JLabel passwordLabel = new JLabel("Jdbc Password");

    JLabel blankLabel = new JLabel(" ");
    JButton submitButton = new JButton("Configure package");
    submitButton.addActionListener(this);

    GridLayout layout = new GridLayout(ConfigurationUI.NUMBER_OF_FIELDS, 2);

    JFrame frame = new JFrame();
    frame.setLayout(layout);
  
    frame.add(driverLabel);
    frame.add(driverField);

    frame.add(urlLabel);
    frame.add(urlField);

    frame.add(userLabel);
    frame.add(usernameField);

    frame.add(passwordLabel);
    frame.add(passwordField);
 
    frame.add(blankLabel);
    frame.add(submitButton);

    frame.pack();
    frame.setVisible(true);
  }

  public void actionPerformed(ActionEvent evt) {
    File temporaryPropertiesOutput = new File("application.properties");
    FileOutputStream os = null;
    try {
      os = new FileOutputStream(temporaryPropertiesOutput);
    } catch (FileNotFoundException e) { }

    Properties databaseProperties = new Properties();
    databaseProperties.put("spring.jpa.hibernate.ddl-auto", "create-drop");
    databaseProperties.put("spring.jpa.hibernate.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
    databaseProperties.put("spring.jpa.show-sql", "true");
    databaseProperties.put("spring.jpa.database", databaseType.getSelectedItem());
    databaseProperties.put("spring.datasource.driver-class-name=", driverField.getSelectedItem());
    databaseProperties.put("spring.datasource.url", urlField.getText());
    databaseProperties.put("spring.datasource.username", userField.getText());
    databaseProperties.put("spring.datasource.password", passwordField.getText());
    databaseProperties.put("spring.datasource.driver-class-name", driverField.getText());


    // TODO see if there's a portable way to get this information
    JarFile jarFile = null;
    try {
      jarFile = new JarFile(System.getProperty("sun.java.command"));
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }

    JarInputStream zipfile = null;
    JarOutputStream zipUpdate = null;
    try {
      zipfile = new JarInputStream(new FileInputStream(jarFile.getName()));
      zipUpdate = new JarOutputStream(new FileOutputStream(jarFile.getName(), true));
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }

    boolean renameOk = new File(jarFile.getName()).renameTo(temporaryPropertiesOutput);
    if (!renameOk) {
      throw new RuntimeException("could not rename the file "+new File(jarFile.getName()).getAbsolutePath()+" to "+temporaryPropertiesOutput.getAbsolutePath());
    }

    Enumeration<JarEntry> entries = jarFile.entries();
    for (JarEntry nextEntry = entries.nextElement(); entries.hasMoreElements(); ) {
      if (nextEntry.getName().contains(temporaryPropertiesOutput.getName())) {
        try {
          zipUpdate.putNextEntry(new JarEntry("/application.properties"));
          byte[] data = new byte[new Long(temporaryPropertiesOutput.length()).intValue()];
          zipfile.read(data);
          zipUpdate.write(data);
        } catch (IOException e) { }
        continue;
      }
    }
    try {
      zipfile.close();
      zipUpdate.close();
    } catch (IOException e) { }
    temporaryPropertiesOutput.delete();
  } 
  
}
