package us.d8u;

import java.awt.HeadlessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {
  private static final Logger log = LoggerFactory.getLogger(RestApiApplication.class);

  public static void main(String[] args) {
    try {
      new ConfigurationUI();
    } catch (HeadlessException e) {
      log.error(e.getMessage());
    }
    SpringApplication.run(RestApiApplication.class, args);
  }
}
