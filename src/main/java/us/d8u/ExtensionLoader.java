package us.d8u;

import java.io.ByteArrayOutputStream;
import java.io.File;  
import java.io.InputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;  
import java.lang.reflect.InvocationTargetException;  
import java.net.MalformedURLException;  
import java.net.URL;  
import java.net.URLClassLoader;

public class ExtensionLoader<C> {
  public C LoadClass(String directory, String remoteUrl, Class<C> parentClass) throws ClassNotFoundException, IOException, MalformedURLException {
    final int BLOCK_SIZE = 32768;

    String classpath = System.getProperty("java.class.path");
    String pluginsPath = System.getProperty("user.dir") + directory;

    URL remoteLocation = new URL(remoteUrl);
    InputStream remoteStream_ = remoteLocation.openStream();
    ByteArrayOutputStream remoteStream = new ByteArrayOutputStream();
    do {
      byte[] b = new byte[BLOCK_SIZE];
      int length = remoteStream_.read(b);
      remoteStream.write(b);
      if (length < BLOCK_SIZE) { 
        break; 
      }
    } while (true);
      
    
    File pluginsDir = new File(pluginsPath);
     
    for (File jar : pluginsDir.listFiles()) {
      try {
        ClassLoader loader = URLClassLoader.newInstance(
            new URL[] { jar.toURI().toURL() },
            getClass().getClassLoader()
        );
        Class<?> clazz = Class.forName(classpath, true, loader);
        Class<? extends C> newClass = clazz.asSubclass(parentClass);
        // Apparently its bad to use Class.newInstance, so we use 
        // newClass.getConstructor() instead
        Constructor<? extends C> constructor = newClass.getConstructor();
        return constructor.newInstance();

      } catch (ClassNotFoundException e) {
        // There might be multiple JARs in the directory,
        // so keep looking
        continue;
      } catch (MalformedURLException e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      } catch (InstantiationException e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      }
    }
    throw new ClassNotFoundException("Class " + classpath
        + " wasn't found in directory " + System.getProperty("user.dir") + directory);
  }
}
