# REST API generator
# By: Hasan Diwan <hasandiwan@gmail.com>
## Instructions

You will need to add a jdbc implementation to pom.xml. You'll need to specify the properties in src/main/java/us/d8u/PersistenceJPAConfig.java when running the application. The application will create entity classes dynamically from the jdbc database specified by the jdbc.url.

This will then encompass an endpoint at the "/rest" namespace, which takes a "table" parameter and a "statement", which correspond to an entity name in the database specified and a statement for what needs to be done. All methods return JSON.org formatted output. 
